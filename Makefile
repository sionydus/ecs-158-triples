CC=gcc
CFLAGS=-fopenmp -g
EXEC=triples
LDFLAGS=

all: $(EXEC)

triples: triples.o main.o
	$(CC) -o triples triples.o main.o $(CFLAGS)

triples.o:
	$(CC) -o triples.o -c triples.c $(CFLAGS)

main.o:
	$(CC) -o main.o -c main.c $(CFLAGS)

clean:
	rm *.o $(EXEC)

.PHONY: clean
