#include <stdio.h>
#include <omp.h>
#include <stdlib.h>

#define DEBUG_PRINT_MATRIX() \
do { \
    int i; \
    for (i = 0; i < n*n; i++) { \
       if(i % n == 0) printf("\n"); \
           printf("%d ",adj[i]); \
   } \
   printf("\n"); \
} while(0)
#define DEBUG_PRINT_TRIPLES() printf("vertices %d, %d and %d are a triple\n", i, j, k)
#define DEBUG_PRINT_NUM_THREADS() printf("There are %d threads running.\n", omp_get_num_threads())
#define DEBUG_PRINT_THREAD_NUM() printf("This is thread %d.\n", omp_get_thread_num())

int triples(int *adj, int n) {
  //    int count = 0;
    long sum = 0;
    int * counts;
    int num_of_threads;

    //DEBUG_PRINT_MATRIX();
    #pragma omp parallel
    {
      int id = omp_get_thread_num();
      num_of_threads = omp_get_num_threads();
      //printf("ID =  %d\n", id); 
       #pragma omp single
       {
         
	  int i;
          counts = malloc(sizeof(int)*num_of_threads);
	  for(i = 0; i < num_of_threads; i++) {
	    //printf("STILL SETTING TO ZERO\n");
	    counts[i] = 0;
	  }
	  
       }
        
        int i, j, k;
        //DEBUG_PRINT_NUM_THREADS();
	//DEBUG_PRINT_THREAD_NUM();
        #pragma omp for schedule(static)
        for (i = 0; i < n-2; i++) {
	  //printf("FOR STARTS FIRST : ID = %d\n", id);
	  //DEBUG_PRINT_THREAD_NUM();
            for (j = i+1; j < n-1; j++) {
                for (k = j+1; k < n; k++) {
                    // consider every thread having own count and summing
                    if (adj[i*n+j] && adj[j*n+k] && adj[k*n+i]) {
                        // DEBUG_PRINT_TRIPLES();
                        // DEBUG_PRINT_THREAD_NUM();
                        counts[id]++;
                    }
                }
            }
        }
    } //end of #parallel
    int id;
    for(id = 0; id < num_of_threads; id++) {
      sum += (long)(counts[id]);
      //printf("COUNT = %d, ID = %d\n", counts[id], id);
    }
    return sum;
}
