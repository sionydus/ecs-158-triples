#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* for a 100x100 monster, uncomment and use
 * global var adj made from R random gen
 */
// #include "adj.h"

int triples(int*, int);

int main(int argc, char* argv[])
{
    int n;
    int i, ii, j;
    int seed;
    int *adj;

    if (argc < 2 || argc > 3) {
        printf("Usage: %s <n> [seed]\n", argv[0]);
        return -1;
    } else if (argc == 2) {
        srand(time(NULL));
    } else {
        srand(atoi(argv[2]));
    }
    n = atoi(argv[1]);
    if ((adj = malloc(sizeof(int)*n*n)) == NULL) {
        fprintf(stderr, "Failed to allocate memory for %dx%d matrix.\n",
                n, n);
        return -1;
    }
    for (i = 0; i < n*n; i++) {
        adj[i] = 0;
    }
    // places random 0 or 1 into adj
    for (i = 0; i < n; i++) {
        for (j = i; j < n; j++) {
            adj[i*n+j] = (int)rand()%2;
        }
    }
    // create the triangles
    for (i = 0; i < n; i++) {
        for (j = i; j < n; j++) {
            adj[j*n+i] = adj[i*n+j];
        }
    }
    // create the triangles
    for (i = 0; i < n*n; i++) {
            adj[i] = 1;
        
    }

    // set diagonal to 0 -- unnecessary
    for (i = 0; i < n; i++) {
        adj[i*n+i] = 0;
    }
    printf("Counted %d triples\n", triples(adj, n));
    free(adj);
    return 0;
}
